# JAzazelBot
[RU](https://gitlab.com/Will0376/jazazelbot/-/blob/master/RREADME.md)

Another bot for Discord that can play music from YouTube.

It uses the [LavaPlayer-fork](https://github.com/Walkyst/lavaplayer-fork) library, so it can play music from the
following services:

- YouTube
- YandexMusic
- SoundCloud
- Bandcamp
- Vimeo
- Twitch streams
- ~~Getyarn~~ (not working)
- Local files (attached files)
- HTTP URLs (mp3, mp4, webm, etc.)

## Requirements

- Java 17
- Discord bot token (Google it if you don't know how to get one)

## Building

1. Clone the repository with the following
   command:
    ```bash
    git clone https://gitlab.com/Will0376/jazazelbot.git
    ```
2. Build the project with the following command:
    ```bash
    ./gradlew build
    ```
   (Make sure you have Java 17 installed on your system.)

3. The .jar file will be located in the build/libs directory.

## Starting the bot

1. Create a file named settings.properties either next to the .jar file.

2. Open the settings.properties file in a text editor and fill in the following line with your Discord bot token:

    ```properties 
    discord.token=<your_discord_bot_token>
    ```
   Replace <your_discord_bot_token> with the actual token for your bot. Ensure that there are no extra spaces or
   characters in the token.

3. Save the settings.properties file.

4. Run the bot by executing the .jar file or the project. Use the appropriate command depending on your setup. For
   example:

    ```bash
    java -jar JAzazel.jar
    ```
   (Make sure you have Java 17 installed on your system and the java command is accessible from the command line.)

5. Once the bot is running, it should be active on your Discord server. Enjoy using your Discord bot!
6. (Optional) You can add a bot to your server using this
   link: https://discord.com/api/oauth2/authorize?client_id=XXYYZZ&permissions=277028538368&scope=bot%20applications.commands
   where `XXYYZZ` must be replaced with the bot ID

PS. 277028538368 - these are the rights that the bot needs for normal operation. Comprises:

- Send Messages
- Send Messages in Threads
- Use Slash Commands
- Connect
- Speak

## Bot Usage

You can control the bot using "Slash Commands" - a command system in Discord available to developers.

In general, all commands have a short but clear description, which can be seen when typing the command in the Discord
chat.

In addition to autocompletion, Discord provides instructions on which variables the current command expects and their
quantity...

In the near future, I plan to implement an old command system based on prefixes, but it's not certain.
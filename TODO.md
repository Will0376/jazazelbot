# JAzazel TODO

- [+] Возможность проигрывать прикрепленные к сообщению аудио/видео файлы
- [+] Поддержка Coub
- [+] Команда очистки чата от ответов бота
- [+] Log4j2 логгирование в файл
- [+] Консольная командная система (Заготовка, я пока что не знаю что туда можно добавить =|)
- [+] Возможность уведомить... всех пользователей об рестарте? А надо ли?
- [] Веб-консоль (с авторизацией)
- [] Ведение статистики (В каком гилде, какие ссылки, тип сервиса, время, дата, и т.д.)
- [] Придумать как обходить возрастную проверку ТыТрубы (Может
  помочь: https://github.com/zerodytrash/Simple-YouTube-Age-Restriction-Bypass)
- [] Поддержка старой командной системы (с префиксом)
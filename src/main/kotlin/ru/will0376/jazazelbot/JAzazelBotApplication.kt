package ru.will0376.jazazelbot

import jakarta.annotation.PostConstruct
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling
import ru.will0376.jazazelbot.utils.GitProperties

val logger = KotlinLogging.logger {}

@SpringBootApplication
@EnableConfigurationProperties
@ConfigurationPropertiesScan
@EnableScheduling
class JAzazelBotApplication {
    @Autowired
    lateinit var gitProperties: GitProperties
    val startTime = System.currentTimeMillis()

    @PostConstruct
    fun init() {
        logger.info { "Started JAzazelBot. ${gitProperties.getStringVersion()}" }
    }
}

fun main(args: Array<String>) {
    runApplication<JAzazelBotApplication>(*args)
}

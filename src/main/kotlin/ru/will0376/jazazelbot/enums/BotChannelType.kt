package ru.will0376.jazazelbot.enums

enum class BotChannelType {
    DM,
    GUILD,
    ALL
}
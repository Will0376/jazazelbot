package ru.will0376.jazazelbot.utils

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

@Configuration
@PropertySource("classpath:buildinfo.properties")
class GitProperties {
    @Value("\${git.branch}")
    var branch: String? = null

    @Value("\${git.build.host}")
    var buildHost: String? = null

    @Value("\${git.build.user.email}")
    var buildUserEmail: String? = null

    @Value("\${git.build.user.name}")
    var buildUserName: String? = null

    @Value("\${git.build.version}")
    var buildVersion: String? = null

    @Value("\${git.closest.tag.commit.count}")
    var closestTagCommitCount: String? = null

    @Value("\${git.closest.tag.name}")
    var closestTagName: String? = null

    @Value("\${git.commit.id}")
    var commitId: String? = null

    @Value("\${git.commit.id.abbrev}")
    var commitIdAbbrev: String? = null

    @Value("\${git.commit.id.describe}")
    var commitIdDescribe: String? = null

    @Value("\${git.commit.message.full}")
    var commitMessageFull: String? = null

    @Value("\${git.commit.message.short}")
    var commitMessageShort: String? = null

    @Value("\${git.commit.time}")
    var commitTime: String? = null

    @Value("\${git.commit.user.email}")
    var commitUserEmail: String? = null

    @Value("\${git.commit.user.name}")
    var commitUserName: String? = null

    @Value("\${git.dirty}")
    var dirty: String? = null

    @Value("\${git.remote.origin.url}")
    var remoteOriginUrl: String? = null

    @Value("\${git.tags}")
    var tags: String? = null

    @Value("\${git.total.commit.count}")
    var totalCommitCount: String? = null
    fun getStringVersion(): String =
        "Version $buildVersion, commit counter: $totalCommitCount, commit time: $commitTime"
}
package ru.will0376.jazazelbot.utils

import net.dv8tion.jda.api.interactions.commands.OptionType

annotation class SlashOption(
    val type: OptionType,
    val name: String,
    val description: String,
    val required: Boolean = false,
    val autoComplete: Boolean = false
)

package ru.will0376.jazazelbot.utils

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

@Configuration
@PropertySource("file:\${user.dir}/settings.properties", ignoreResourceNotFound = true)
class AppConfig {
    @Value("\${discord.token:#{null}}")
    val discordToken: String? = null
}
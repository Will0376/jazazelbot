package ru.will0376.jazazelbot.utils

import org.springframework.stereotype.Component
import ru.will0376.jazazelbot.enums.BotChannelType

@Component
annotation class SlashCommand(
    val name: String,
    val description: String,
    val options: Array<SlashOption> = [],
    val channelType: BotChannelType = BotChannelType.GUILD
)


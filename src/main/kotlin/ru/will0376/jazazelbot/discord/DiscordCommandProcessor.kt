package ru.will0376.jazazelbot.discord

import net.dv8tion.jda.api.events.guild.GuildReadyEvent
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import net.dv8tion.jda.api.events.session.ReadyEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.will0376.jazazelbot.discord.context.ContextData
import ru.will0376.jazazelbot.discord.context.DiscordCommandContextHolder
import ru.will0376.jazazelbot.discord.context.SlashCommandEventContext
import ru.will0376.jazazelbot.discord.slashs.ISlashCommand
import ru.will0376.jazazelbot.discord.slashs.getGuildId
import ru.will0376.jazazelbot.logger
import ru.will0376.jazazelbot.music.GuildPlayerManager
import ru.will0376.jazazelbot.utils.SlashCommand
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.ThreadFactory
import java.util.concurrent.atomic.AtomicInteger
import kotlin.reflect.full.findAnnotation


@Component
class DiscordCommandProcessor : ListenerAdapter() {

    @Autowired
    private lateinit var guildPlayerManager: GuildPlayerManager

    @Autowired
    lateinit var slashClassList: List<ISlashCommand>


    //TODO: Реализовать возможность принудительно прибить все треды в пуле в гильдии
    private val guildExecutors: MutableMap<Long, ExecutorService> = ConcurrentHashMap()

    override fun onSlashCommandInteraction(event: SlashCommandInteractionEvent) {
        val guildId = event.guild!!.idLong

        val executorService = guildExecutors.computeIfAbsent(guildId) {
            Executors.newFixedThreadPool(2, GuildThreadFactory(guildId))
        }

        logger.info { "Slash command: ${event.name}" }
        val filteredCommand = slashClassList.find { command ->
            val slashCommandAnnotation = command::class.findAnnotation<SlashCommand>()
                ?: throw IllegalStateException("Command class is not annotated with SlashCommand")

            slashCommandAnnotation.name == event.name
        }
        executorService.submit {
            try {
                filteredCommand?.let {
                    DiscordCommandContextHolder.setContext(
                        SlashCommandEventContext(
                            createContextData(event)
                        )
                    )
                    try {
                        it.handle()
                    } catch (e: Exception) {
                        event.hook.editOriginal("ERROR").queue()
                        throw e
                    }
                } ?: event.reply("Unknown command").queue()
            } catch (e: Exception) {
                logger.error(e) { "Error while executing slash command" }
                event.channel.sendMessage("Error while executing command: ${e.message}").queue()
            } finally {
                DiscordCommandContextHolder.clearContext()
            }
        }
    }

    fun createContextData(event: SlashCommandInteractionEvent): ContextData {
        val audioPlayerManager = guildPlayerManager.guildPlayerManagers[event.getGuildId()]
        val audioPlayer = guildPlayerManager.guildPlayers[event.getGuildId()]
        val trackScheduler = guildPlayerManager.guildSchedulers[event.getGuildId()]
        return ContextData(event, audioPlayerManager, trackScheduler, audioPlayer)
    }
    override fun onReady(event: ReadyEvent) {
        logger.info { "Discord bot is ready" }
    }

    override fun onGuildReady(event: GuildReadyEvent) {
        logger.info { "Guild ready: ${event.guild.name} with ${event.guild.memberCount} members" }
    }

    class GuildThreadFactory(private val guildId: Long) : ThreadFactory {
        private val threadNumber = AtomicInteger(1)
        override fun newThread(runnable: Runnable): Thread {
            return Thread(runnable, "guild-$guildId-thread-${threadNumber.getAndIncrement()}")
        }
    }
}
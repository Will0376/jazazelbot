package ru.will0376.jazazelbot.discord.utils

data class PlayArgs(
    val url: String,
    val hide: Boolean = false,
    val force: Boolean = false,
    val playlistSize: Int = 10,
    val repeat: Boolean = false
)

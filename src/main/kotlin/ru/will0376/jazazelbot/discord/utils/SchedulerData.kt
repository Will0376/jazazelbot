package ru.will0376.jazazelbot.discord.utils

import net.dv8tion.jda.api.entities.channel.unions.MessageChannelUnion

data class SchedulerData(
    val guildId: Long,
    val channel: MessageChannelUnion,
    var runnableBlock: (() -> Unit)? = null,
    var timerThread: Thread? = null
) {
    fun printToChannel(message: String, disableEmbedded: Boolean = false) {
        channel.sendMessage(message).setSuppressEmbeds(disableEmbedded).queue()
    }
}

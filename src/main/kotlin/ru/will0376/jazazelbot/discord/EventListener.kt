package ru.will0376.jazazelbot.discord

import net.dv8tion.jda.api.events.guild.GuildJoinEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberRemoveEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.springframework.stereotype.Component
import ru.will0376.jazazelbot.logger

@Component
class EventListener : ListenerAdapter() {
    override fun onGuildJoin(event: GuildJoinEvent) {
        logger.info { "Joined to guild ${event.guild.name}" }
    }

    override fun onGuildMemberJoin(event: GuildMemberJoinEvent) {
        logger.info { "Member ${event.member.effectiveName} joined guild ${event.guild.name}" }
        super.onGuildMemberJoin(event)
    }

    override fun onGuildMemberRemove(event: GuildMemberRemoveEvent) {
        logger.info { "Member ${event.member?.effectiveName} left guild ${event.guild.name}" }
        super.onGuildMemberRemove(event)
    }
}
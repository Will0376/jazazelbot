package ru.will0376.jazazelbot.discord.slashs

import org.springframework.beans.factory.annotation.Autowired
import ru.will0376.jazazelbot.discord.DiscordService
import ru.will0376.jazazelbot.logger
import ru.will0376.jazazelbot.music.GuildPlayerManager
import ru.will0376.jazazelbot.utils.SlashCommand

@SlashCommand(name = "stop", description = "Stop music")
class StopCommand : ISlashCommand {
    @Autowired
    lateinit var guildPlayerManager: GuildPlayerManager

    @Autowired
    lateinit var discordService: DiscordService

    override fun handle() {
        val guildId = getContext().getGuildId()
        stop(guildId)
        sendReply("Bye!")
    }

    fun stop(guild: Long) {
        discordService.jda.getGuildById(guild)?.let {
            guildPlayerManager.guildPlayers.remove(guild)
            guildPlayerManager.guildSchedulers.remove(guild)
            it.audioManager.closeAudioConnection()
            logger.info { "Bot disconnected from $guild guild due to timeout" }
        }
    }
}
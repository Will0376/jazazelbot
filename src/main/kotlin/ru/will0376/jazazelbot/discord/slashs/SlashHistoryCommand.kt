package ru.will0376.jazazelbot.discord.slashs

import net.dv8tion.jda.api.interactions.commands.OptionType
import org.springframework.beans.factory.annotation.Autowired
import ru.will0376.jazazelbot.discord.utils.PlayArgs
import ru.will0376.jazazelbot.music.GuildPlayerManager
import ru.will0376.jazazelbot.utils.SlashCommand
import ru.will0376.jazazelbot.utils.SlashOption

@SlashCommand(
    name = "history",
    description = "Print history",
    options = [SlashOption(OptionType.INTEGER, "re-add", "Re-add from history")]
)
class SlashHistoryCommand : ISlashCommand {
    @Autowired
    lateinit var guildPlayerManager: GuildPlayerManager

    @Autowired
    lateinit var playCommand: PlayCommand
    override fun handle() {
        guildPlayerManager.findScheduler(getContext().getGuildId()).let { scheduler ->
            if (scheduler.playedHistory.isEmpty()) sendReply("History is empty")
            else if (hasOption("re-add")) {
                setDeferReply()
                getOption<Int>("re-add")!!.let {
                    scheduler.playedHistory[scheduler.playedHistory.size.coerceIn(
                        0,
                        it - 1
                    )].let { trackInfo ->
                        playCommand.playSong(
                            PlayArgs(
                                trackInfo.uri,
                                trackInfo.audioTrackData.hidden
                            )
                        )
                    }
                }
            } else {
                sendReply(scheduler.playedHistory.map {
                    "[${scheduler.playedHistory.indexOf(it) + 1}] ${if (it.audioTrackData.hidden) "Hidden" else it.title} -> ${it.uri}"
                }.joinToString(separator = "") { "$it\n" }, true)
            }
        }
    }
}
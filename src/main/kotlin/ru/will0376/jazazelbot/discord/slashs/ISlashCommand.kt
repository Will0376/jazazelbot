package ru.will0376.jazazelbot.discord.slashs

import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.entities.channel.unions.MessageChannelUnion
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import ru.will0376.jazazelbot.discord.context.DiscordCommandContextHolder
import ru.will0376.jazazelbot.discord.context.IDiscordCommandContext

interface ISlashCommand {
    fun handle()
    fun getContext(): IDiscordCommandContext = DiscordCommandContextHolder.getContext()
    fun sendReply(text: String, suppressEmbeds: Boolean = false) =
        getContext().sendReply(text, suppressEmbeds)

    fun sendMessage(text: String, suppressEmbeds: Boolean = false) =
        getContext().sendMessage(text, suppressEmbeds)

    fun setDeferReply() = getContext().setDeferReply()
    fun replyEmbeds(embed: MessageEmbed) = getContext().replyEmbeds(embed)
    fun editLastMessage(text: String) = getContext().editLastMessage(text)
    fun getChannel() = getContext().getJDA()
        .getChannelById(MessageChannelUnion::class.java, getContext().getChannelId())!!

    fun hasOption(name: String): Boolean = getContext().hasOption(name)
}

inline fun <reified T> ISlashCommand.getOption(name: String): T? {
    return getContext().getOption(name, T::class.java)
}

fun SlashCommandInteractionEvent.getGuildId(): Long =
    if (this.isFromGuild) this.guild!!.idLong else throw IllegalStateException("Event is not from guild")

fun SlashCommandInteractionEvent.hasOption(name: String): Boolean = this.getOption(name) != null

package ru.will0376.jazazelbot.discord.slashs

import net.dv8tion.jda.api.interactions.commands.OptionType
import ru.will0376.jazazelbot.utils.SlashCommand
import ru.will0376.jazazelbot.utils.SlashOption

@SlashCommand(
    "clean", "Clean bot messages from channel", options = [SlashOption(
        name = "count",
        description = "Count of messages to delete(default 50)",
        type = OptionType.INTEGER
    )]
)
class CleanCommand : ISlashCommand {
    override fun handle() {
        sendReply("Clean in progress")
        val option = getOption<Int>("count") ?: 50
        purgeMessages(option)
    }

    private fun purgeMessages(option: Int) {
        val batchSize = minOf(option, 100)
        val selfUserId = getContext().getJDA().selfUser.id
        getChannel().history.retrievePast(batchSize).queue { messages ->
            val messagesToDelete = messages.filter { it.author.id == selfUserId }

            if (messagesToDelete.isNotEmpty()) {
                getChannel().purgeMessages(messagesToDelete)
            }

            val remainingOption = option - batchSize
            if (remainingOption > 0) {
                purgeMessages(remainingOption)
            }
        }
    }
}
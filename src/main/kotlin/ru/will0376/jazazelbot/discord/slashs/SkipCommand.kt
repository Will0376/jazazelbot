package ru.will0376.jazazelbot.discord.slashs

import net.dv8tion.jda.api.interactions.commands.OptionType
import org.springframework.beans.factory.annotation.Autowired
import ru.will0376.jazazelbot.utils.SlashCommand
import ru.will0376.jazazelbot.utils.SlashOption

@SlashCommand(
    name = "skip",
    description = "Skip current song",
    options = [SlashOption(
        name = "skiprepeat",
        description = "Skip repeat",
        type = OptionType.BOOLEAN
    )]
)
class SkipCommand : ISlashCommand {
    @Autowired
    lateinit var playNextCommand: PlayNextCommand
    override fun handle() {
        playNextCommand.handle()
    }
}
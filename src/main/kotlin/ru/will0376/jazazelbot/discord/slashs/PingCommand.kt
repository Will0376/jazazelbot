package ru.will0376.jazazelbot.discord.slashs

import ru.will0376.jazazelbot.utils.SlashCommand

@SlashCommand("ping", "ping command")
class PingCommand : ISlashCommand {
    override fun handle() {
        sendReply("Pong: ${getContext().getJDA().gatewayPing} ms")
    }
}
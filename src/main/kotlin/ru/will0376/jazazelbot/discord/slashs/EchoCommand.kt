package ru.will0376.jazazelbot.discord.slashs

import net.dv8tion.jda.api.interactions.commands.OptionType
import ru.will0376.jazazelbot.enums.BotChannelType
import ru.will0376.jazazelbot.utils.SlashCommand
import ru.will0376.jazazelbot.utils.SlashOption

@SlashCommand(
    name = "echo",
    description = "Echo",
    options = [SlashOption(OptionType.STRING, "text", "Text", true)],
    channelType = BotChannelType.ALL
)
class EchoCommand : ISlashCommand {
    override fun handle() {
        sendReply("Echo: ${getOption<String>("text")}")
    }
}
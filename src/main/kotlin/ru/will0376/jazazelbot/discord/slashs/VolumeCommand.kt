package ru.will0376.jazazelbot.discord.slashs

import net.dv8tion.jda.api.interactions.commands.OptionType
import org.springframework.beans.factory.annotation.Autowired
import ru.will0376.jazazelbot.logger
import ru.will0376.jazazelbot.music.GuildPlayerManager
import ru.will0376.jazazelbot.utils.SlashCommand
import ru.will0376.jazazelbot.utils.SlashOption

@SlashCommand(
    name = "volume", description = "Set volume", options = [
        SlashOption(OptionType.STRING, "volume", "volume", true)
    ]
)
class VolumeCommand : ISlashCommand {
    @Autowired
    lateinit var guildPlayerManager: GuildPlayerManager
    override fun handle() {
        val option = getOption<String>("volume")!!
        val audioPlayer = guildPlayerManager.findPlayer(getContext().getGuildId())
        try {
            when (option[0]) {
                '+' -> {
                    option.split('+')[1].toInt().let {
                        audioPlayer.volume += it
                        sendReply("Volume set to ${audioPlayer.volume}")
                    }
                }

                '-' -> {
                    option.split('-')[1].toInt().let {
                        audioPlayer.volume -= it
                        sendReply("Volume set to ${audioPlayer.volume}")
                    }
                }

                else -> {
                    option.toInt().let {
                        audioPlayer.volume = it
                        sendReply("Volume set to ${audioPlayer.volume}")
                    }
                }
            }
        } catch (e: Exception) {
            logger.catching(e)
            sendReply("Invalid volume")
        }
    }
}
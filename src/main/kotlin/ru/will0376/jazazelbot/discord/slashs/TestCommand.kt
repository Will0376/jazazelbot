package ru.will0376.jazazelbot.discord.slashs

import net.dv8tion.jda.api.exceptions.ContextException
import ru.will0376.jazazelbot.utils.SlashCommand

@SlashCommand(name = "test", description = "Test command")
class TestCommand : ISlashCommand {
    override fun handle() {
        sendReply("Started long command")
        try {
            for (i in 0..100000) {
                Thread.sleep(1000)
                editLastMessage("Current number: $i")
            }
        } catch (e: ContextException) {
            sendReply("Command was cancelled")
        }
    }
}
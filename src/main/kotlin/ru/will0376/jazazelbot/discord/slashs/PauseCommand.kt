package ru.will0376.jazazelbot.discord.slashs

import org.springframework.beans.factory.annotation.Autowired
import ru.will0376.jazazelbot.music.GuildPlayerManager
import ru.will0376.jazazelbot.utils.SlashCommand

@SlashCommand(name = "pause", description = "Pause/Resume music")
class PauseCommand : ISlashCommand {
    @Autowired
    lateinit var guildPlayerManager: GuildPlayerManager
    override fun handle() {
        val guildId = getContext().getGuildId()
        guildPlayerManager.findScheduler(guildId).let {
            it.player.isPaused = !it.player.isPaused
            sendReply("Player ${if (it.player.isPaused) "paused" else "resumed"}")
        }
    }
}
package ru.will0376.jazazelbot.discord.slashs

import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.interactions.commands.OptionType
import org.springframework.beans.factory.annotation.Autowired
import ru.will0376.jazazelbot.discord.utils.PlayArgs
import ru.will0376.jazazelbot.utils.SlashCommand
import ru.will0376.jazazelbot.utils.SlashOption

@SlashCommand(
    name = "playattached", description = "Play attached file", options = [
        SlashOption(
            name = "file",
            description = "File to play",
            type = OptionType.ATTACHMENT,
            required = true
        ),
        SlashOption(name = "hide", description = "Hide song", type = OptionType.BOOLEAN)
    ]
)
class PlayAttachedCommand : ISlashCommand {
    @Autowired
    lateinit var playCommand: PlayCommand
    override fun handle() {
        setDeferReply()
        val option = getOption<Message.Attachment>("file")!!
        playCommand.playSong(PlayArgs(option.url))
    }
}

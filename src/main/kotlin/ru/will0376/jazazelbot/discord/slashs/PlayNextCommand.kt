package ru.will0376.jazazelbot.discord.slashs

import net.dv8tion.jda.api.interactions.commands.OptionType
import org.springframework.beans.factory.annotation.Autowired
import ru.will0376.jazazelbot.discord.utils.PlayArgs
import ru.will0376.jazazelbot.music.GuildPlayerManager
import ru.will0376.jazazelbot.music.MusicLoadResultHandler
import ru.will0376.jazazelbot.music.getAudioData
import ru.will0376.jazazelbot.utils.SlashCommand
import ru.will0376.jazazelbot.utils.SlashOption

@SlashCommand(
    name = "playnext", description = "Play next song", options = [SlashOption(
        name = "url", description = "URL of song", type = OptionType.STRING
    ), SlashOption(
        name = "hide", description = "Hide song", type = OptionType.BOOLEAN
    ), SlashOption(
        name = "playlistsize", description = "Playlist size(-1 = all)", type = OptionType.INTEGER
    ), SlashOption(
        name = "repeat", description = "Repeat song", type = OptionType.BOOLEAN
    ), SlashOption(
        name = "skiprepeat", description = "Skip repeat", type = OptionType.BOOLEAN
    )]
)
class PlayNextCommand : ISlashCommand {
    @Autowired
    lateinit var guildPlayerManager: GuildPlayerManager
    override fun handle() {
        setDeferReply()
        val guildId = getContext().getGuildId()
        val trackScheduler = guildPlayerManager.findScheduler(guildId)
        val hide = getOption("hide") ?: false
        val playlistSize = getOption("playlistsize") ?: 10
        val repeat = getOption("repeat") ?: false
        val skipRepeat = getOption("skiprepeat") ?: false
        getOption<String>("url")?.let { url ->
            guildPlayerManager.getOrInitPlayerManager(guildId).loadItem(url, MusicLoadResultHandler(
                trackScheduler, PlayArgs(url, hide, false, playlistSize, repeat)
            ) {
                trackScheduler.queue.let { list ->
                    val removeLast = list.removeLast()
                    list.add(0, removeLast)
                }
            })

        } ?: run {
            trackScheduler.player.playingTrack?.let {
                editLastMessage("Skipped '${if (it.getAudioData().hidden) "Hidden" else it.info.title}'")
            } ?: run {
                editLastMessage("Nothing to skip")
            }
            trackScheduler.playNext(skipRepeat)
        }
    }
}
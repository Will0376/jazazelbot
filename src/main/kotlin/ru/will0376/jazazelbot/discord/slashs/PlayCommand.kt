package ru.will0376.jazazelbot.discord.slashs

import net.dv8tion.jda.api.interactions.commands.OptionType
import org.springframework.beans.factory.annotation.Autowired
import ru.will0376.jazazelbot.discord.utils.PlayArgs
import ru.will0376.jazazelbot.logger
import ru.will0376.jazazelbot.music.GuildPlayerManager
import ru.will0376.jazazelbot.music.MusicLoadResultHandler
import ru.will0376.jazazelbot.utils.SlashCommand
import ru.will0376.jazazelbot.utils.SlashOption

@SlashCommand(
    name = "play", description = "Play music", options = [
        SlashOption(
            type = OptionType.STRING,
            name = "url",
            description = "Url",
            required = true
        ), SlashOption(name = "hide", description = "Hide song", type = OptionType.BOOLEAN),
        SlashOption(name = "force", description = "Force play", type = OptionType.BOOLEAN),
        SlashOption(
            name = "playlistsize",
            description = "Playlist size(-1 = all)",
            type = OptionType.INTEGER
        ),
        SlashOption(name = "repeat", description = "Repeat song", type = OptionType.BOOLEAN)
    ]
)
class PlayCommand : ISlashCommand {
    @Autowired
    lateinit var guildPlayerManager: GuildPlayerManager

    @Autowired
    lateinit var connectCommand: ConnectCommand
    override fun handle() {
        setDeferReply()
        val url = getOption<String>("url")!!
        val hide = getOption<Boolean>("hide") ?: false
        val force = getOption<Boolean>("force") ?: false
        val playlistSize = getOption<Int>("playlistsize") ?: 10
        val repeat = getOption<Boolean>("repeat") ?: false
        playSong(PlayArgs(url, hide, force, playlistSize, repeat))
    }

    fun playSong(playArgs: PlayArgs) {
        try {
            val trackScheduler =
                if (playArgs.force) guildPlayerManager.findScheduler(getContext().getGuildId()) else connectCommand.initPlayer()
            if (!playArgs.force) {
                val voiceChannel = connectCommand.findVoiceChannel()
                connectCommand.connectToVoiceChannel(trackScheduler.player, voiceChannel)
            }
            guildPlayerManager.getOrInitPlayerManager(getContext().getGuildId())
                .loadItem(
                    playArgs.url,
                    MusicLoadResultHandler(
                        trackScheduler,
                        playArgs
                    )
                )
        } catch (e: Exception) {
            logger.catching(e)
            editLastMessage("Error: ${e.message}")
        }
    }
}
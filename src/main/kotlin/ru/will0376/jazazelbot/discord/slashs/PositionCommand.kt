package ru.will0376.jazazelbot.discord.slashs

import net.dv8tion.jda.api.interactions.commands.OptionType
import org.springframework.beans.factory.annotation.Autowired
import ru.will0376.jazazelbot.music.GuildPlayerManager
import ru.will0376.jazazelbot.utils.SlashCommand
import ru.will0376.jazazelbot.utils.SlashOption
import java.util.concurrent.TimeUnit
import kotlin.math.max
import kotlin.math.min

@SlashCommand(
    name = "position",
    description = "Print the position of the track",
    options = [SlashOption(OptionType.INTEGER, "forward", "Fast forward (sec)"), SlashOption(
        OptionType.INTEGER, "rewind", "Fast rewind (sec)"
    )]
)
class PositionCommand : ISlashCommand {
    @Autowired
    lateinit var guildPlayerManager: GuildPlayerManager
    override fun handle() {
        val scheduler = guildPlayerManager.findScheduler(getContext().getGuildId())
        if (hasOption("forward")) {
            val asInt = getOption<Int>("forward")!!
            scheduler.player.playingTrack.position = min(
                scheduler.player.playingTrack.duration,
                scheduler.player.playingTrack.position + TimeUnit.SECONDS.toMillis(asInt.toLong())
            )
        } else if (hasOption("rewind")) {
            val asInt = getOption<Int>("rewind")!!
            scheduler.player.playingTrack.position = max(
                0,
                scheduler.player.playingTrack.position - TimeUnit.SECONDS.toMillis(asInt.toLong())
            )
        }
        sendReply(
            "${millisToReadableTime(scheduler.player.playingTrack.position)} / ${
                millisToReadableTime(
                    scheduler.player.playingTrack.duration
                )
            }"
        )
    }

    fun millisToReadableTime(millis: Long): String {
        val hours = TimeUnit.MILLISECONDS.toHours(millis)
        val minutes = TimeUnit.MILLISECONDS.toMinutes(millis) % 60
        val seconds = TimeUnit.MILLISECONDS.toSeconds(millis) % 60
        val sb = StringBuilder()

        if (hours > 0) {
            sb.append(hours)
            sb.append(":")
        }
        if (minutes < 10) {
            sb.append("0")
        }
        sb.append(minutes)
        sb.append(":")
        if (seconds < 10) {
            sb.append("0")
        }
        sb.append(seconds)

        return sb.toString()
    }

    //TODO: Доделать конвертацию в миллисекунды
    fun readableTimeToMillis(time: String): Long {
        val parts = time.split(":")
        return when (parts.size) {
            2 -> TimeUnit.MINUTES.toMillis(parts[0].toLong()) + TimeUnit.SECONDS.toMillis(parts[1].toLong())
            3 -> TimeUnit.HOURS.toMillis(parts[0].toLong()) + TimeUnit.MINUTES.toMillis(parts[1].toLong()) + TimeUnit.SECONDS.toMillis(
                parts[2].toLong()
            )
            else -> return -1L
        }
    }
}
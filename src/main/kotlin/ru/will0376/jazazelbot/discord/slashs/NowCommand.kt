package ru.will0376.jazazelbot.discord.slashs

import org.springframework.beans.factory.annotation.Autowired
import ru.will0376.jazazelbot.music.GuildPlayerManager
import ru.will0376.jazazelbot.music.getAudioData
import ru.will0376.jazazelbot.utils.SlashCommand

@SlashCommand(name = "now", description = "Print the track currently playing")
class NowCommand : ISlashCommand {
    @Autowired
    lateinit var guildPlayerManager: GuildPlayerManager

    @Autowired
    lateinit var positionCommand: PositionCommand
    override fun handle() {
        guildPlayerManager.findScheduler(getContext().getGuildId()).let { scheduler ->
            if (scheduler.player.playingTrack == null) {
                sendReply("Nothing playing", true)
                return
            }
            sendReply(
                "Now playing '${scheduler.player.playingTrack?.let { if (it.getAudioData().hidden) "Hidden" else it.info.title }}' -> ${scheduler.player.playingTrack.info.uri}\nVolume: ${scheduler.player.volume}%\nPosition: ${
                    positionCommand.millisToReadableTime(
                        scheduler.player.playingTrack.position
                    )
                } / ${positionCommand.millisToReadableTime(scheduler.player.playingTrack.duration)}",
                true
            )
        }
    }
}
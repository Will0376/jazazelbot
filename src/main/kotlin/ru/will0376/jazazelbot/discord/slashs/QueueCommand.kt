package ru.will0376.jazazelbot.discord.slashs

import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import net.dv8tion.jda.api.interactions.commands.OptionType
import org.springframework.beans.factory.annotation.Autowired
import ru.will0376.jazazelbot.music.GuildPlayerManager
import ru.will0376.jazazelbot.music.getAudioData
import ru.will0376.jazazelbot.utils.SlashCommand
import ru.will0376.jazazelbot.utils.SlashOption

@SlashCommand(
    name = "queue",
    description = "Print the queue",
    options = [SlashOption(OptionType.STRING, "clear", "Clear the queue"), SlashOption(
        OptionType.INTEGER, "remove", "Remove track from queue"
    )]
)
class QueueCommand : ISlashCommand {
    @Autowired
    lateinit var guildPlayerManager: GuildPlayerManager
    override fun handle() {
        guildPlayerManager.findScheduler(getContext().getGuildId()).let { scheduler ->
            getOption<String>("clear")?.let {
                scheduler.queue.clear()
                sendReply("Queue cleared")
                return
            }
            getOption<Long>("remove")?.let {
                if (removeElement(scheduler.queue, it.toInt() - 1)) {
                    sendReply("Track removed")
                } else {
                    sendReply("Track not found")
                }
                return
            }
            if (scheduler.queue.isEmpty()) sendReply("Queue is empty")
            else {
                setDeferReply()
                editLastMessage("Queue:")
                scheduler.queue.map { "[${scheduler.queue.indexOf(it) + 1}] ${if (it.getAudioData().hidden) "Hidden" else it.info.title} -> ${it.info.uri}" }
                    .chunked(5).forEach {
                        sendMessage(it.joinToString(separator = "\n"), true)
                    }
            }
        }
    }

    fun removeElement(queue: MutableList<AudioTrack>, index: Int): Boolean {
        if (index >= 0 && index < queue.size) {
            queue.removeAt(index)
            return true
        }
        return false
    }
}
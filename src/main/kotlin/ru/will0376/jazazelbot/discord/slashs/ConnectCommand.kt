package ru.will0376.jazazelbot.discord.slashs

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import net.dv8tion.jda.api.entities.channel.ChannelType
import net.dv8tion.jda.api.entities.channel.concrete.VoiceChannel
import net.dv8tion.jda.api.entities.channel.unions.GuildChannelUnion
import net.dv8tion.jda.api.interactions.commands.OptionType
import org.springframework.beans.factory.annotation.Autowired
import ru.will0376.jazazelbot.discord.utils.SchedulerData
import ru.will0376.jazazelbot.music.GuildPlayerManager
import ru.will0376.jazazelbot.music.MusicAudioSendHandler
import ru.will0376.jazazelbot.music.TrackScheduler
import ru.will0376.jazazelbot.utils.SlashCommand
import ru.will0376.jazazelbot.utils.SlashOption

@SlashCommand(
    "connect", "Connect to voice channel", options = [SlashOption(
        OptionType.CHANNEL, "channel", "Voice channel", false, autoComplete = false
    )]
)
class ConnectCommand : ISlashCommand {
    @Autowired
    lateinit var guildPlayerManager: GuildPlayerManager

    @Autowired
    lateinit var stopCommand: StopCommand

    var restartPlanned = false
    override fun handle() {
        if (restartPlanned) {
            sendReply("Restarting...")
            return
        }
        val findVoiceChannel = if (hasOption("channel")) {
            val asChannel = getOption<GuildChannelUnion>("channel")!!
            if (asChannel.type != ChannelType.VOICE) throw RuntimeException("Channel is not voice")
            asChannel.asVoiceChannel()
        } else findVoiceChannel()
        val trackScheduler = initPlayer()
        connectToVoiceChannel(trackScheduler.player, findVoiceChannel)
        sendReply("Connected to ${findVoiceChannel.name}")
    }

    fun findVoiceChannel(): VoiceChannel {
        val member = getContext().getCalledUser() ?: throw RuntimeException("Member is null")
        return getContext().getGuild().voiceChannels.firstOrNull { it.members.contains(member) }
            ?: run {
                throw RuntimeException("Member not in voice channel")
            }
    }

    fun initPlayer(): TrackScheduler {
        val player = guildPlayerManager.guildPlayers.getOrPut(getContext().getGuildId()) {
            val player =
                guildPlayerManager.getOrInitPlayerManager(getContext().getGuildId()).createPlayer()
            player
        }

        val trackScheduler =
            guildPlayerManager.guildSchedulers.getOrPut(getContext().getGuildId()) {
                val trackScheduler = TrackScheduler(player)
                player.addListener(trackScheduler)
                trackScheduler
            }
        trackScheduler.schedulerData = if (trackScheduler.dataIsInit()) createSchedulerData(
            trackScheduler.schedulerData.runnableBlock, trackScheduler.schedulerData.timerThread
        ) else {
            val guild = getContext().getGuildId()
            createSchedulerData({
                stopCommand.stop(guild)
            }, null)
        }

        return trackScheduler
    }

    fun connectToVoiceChannel(
        player: AudioPlayer, findVoiceChannel: VoiceChannel
    ) {
        getContext().getGuild().let {
            it.audioManager.openAudioConnection(findVoiceChannel)
            it.audioManager.sendingHandler = MusicAudioSendHandler(player)
        }
    }

    fun createSchedulerData(runnableBlock: (() -> Unit)?, timerThread: Thread?): SchedulerData =
        SchedulerData(
            getContext().getGuildId(), getContext().getChannel(), runnableBlock, timerThread
        )
}
package ru.will0376.jazazelbot.discord.slashs

import net.dv8tion.jda.api.EmbedBuilder
import org.springframework.beans.factory.annotation.Autowired
import ru.will0376.jazazelbot.JAzazelBotApplication
import ru.will0376.jazazelbot.music.GuildPlayerManager
import ru.will0376.jazazelbot.utils.GitProperties
import ru.will0376.jazazelbot.utils.SlashCommand
import java.util.concurrent.TimeUnit

@SlashCommand("status", "Print the status of the bot")
class StatusCommand : ISlashCommand {
    @Autowired
    lateinit var pingCommand: PingCommand

    @Autowired
    lateinit var gitProperties: GitProperties

    @Autowired
    lateinit var guildPlayerManager: GuildPlayerManager

    @Autowired
    lateinit var jAzazelBotApplication: JAzazelBotApplication
    override fun handle() {
        replyEmbeds(getEmbed())
    }

    fun getEmbed() = EmbedBuilder().let {
        it.addField("Guilds", "${getContext().getJDA().guildCache.size()}", true)
        it.addField("AudioPlayers", "${guildPlayerManager.guildPlayers.size}", true)
        it.addField("Ping", "${getContext().getJDA().gatewayPing} ms", true)
        it.addField(
            "Memory",
            "${
                (Runtime.getRuntime().totalMemory() - Runtime.getRuntime()
                    .freeMemory()) / 1024 / 1024
            }MB / ${Runtime.getRuntime().maxMemory() / 1024 / 1024}MB",
            true
        )
        it.addField("Uptime", getUptime(), true)
        it.addField(
            "Version",
            "${gitProperties.buildVersion}.${gitProperties.totalCommitCount}",
            true
        )
        it.setAuthor(
            "JAzazelBot",
            "https://gitlab.com/Will0376/jazazelbot",
            "https://images-ext-1.discordapp.net/external/Oisi6nljBBEfk6OE8clx_KjvKDfZav3Eibs0tYE3ejU/%3Fsize%3D512/https/cdn.discordapp.com/avatars/1115883402692939777/2a0cfaf0a181ce063ae53c4b7482b6c6.png"
        )
        it.setFooter("Author: Will0376")
        it.build()
    }
    fun getUptime(): String {
        val uptime = System.currentTimeMillis() - jAzazelBotApplication.startTime
        val days = TimeUnit.MILLISECONDS.toDays(uptime)
        var remainingTime = uptime - TimeUnit.DAYS.toMillis(days)
        val hours = TimeUnit.MILLISECONDS.toHours(remainingTime)
        remainingTime -= TimeUnit.HOURS.toMillis(hours)
        val minutes = TimeUnit.MILLISECONDS.toMinutes(remainingTime)
        remainingTime -= TimeUnit.MINUTES.toMillis(minutes)
        val seconds = TimeUnit.MILLISECONDS.toSeconds(remainingTime)

        val formattedUptime = StringBuilder()
        if (days > 0) {
            formattedUptime.append("$days days, ")
        }
        if (hours > 0) {
            formattedUptime.append("$hours hours, ")
        }
        if (minutes > 0) {
            formattedUptime.append("$minutes minutes, ")
        }
        formattedUptime.append("$seconds seconds")

        return formattedUptime.toString()
    }
}
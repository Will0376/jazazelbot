package ru.will0376.jazazelbot.discord.context

object DiscordCommandContextHolder {
    private val contextHolder: ThreadLocal<IDiscordCommandContext> =
        ThreadLocal<IDiscordCommandContext>()

    fun getContext(): IDiscordCommandContext {
        return contextHolder.get()
    }

    fun setContext(context: IDiscordCommandContext) {
        contextHolder.set(context)
    }

    fun clearContext() {
        contextHolder.remove()
    }
}

fun getContext() = DiscordCommandContextHolder.getContext()
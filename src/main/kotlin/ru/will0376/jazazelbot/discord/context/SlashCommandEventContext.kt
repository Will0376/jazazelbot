package ru.will0376.jazazelbot.discord.context

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.entities.channel.unions.GuildChannelUnion
import net.dv8tion.jda.api.entities.channel.unions.MessageChannelUnion
import net.dv8tion.jda.api.interactions.commands.OptionMapping
import ru.will0376.jazazelbot.music.TrackScheduler
import java.lang.reflect.Type

class SlashCommandEventContext(private val data: ContextData) : IDiscordCommandContext {
    override fun getGuildId() =
        data.eventContext!!.guild?.idLong ?: throw IllegalStateException("Guild is null")

    override fun getGuild(): Guild =
        data.eventContext!!.guild ?: throw IllegalStateException("Guild is null")

    override fun getChannelId() = data.eventContext!!.channel.idLong
    override fun getChannel(): MessageChannelUnion = data.eventContext!!.channel
    override fun getJDA() = data.eventContext!!.jda
    override fun setDeferReply() = data.eventContext!!.deferReply().queue()
    override fun sendReply(text: String, suppressEmbeds: Boolean) =
        data.eventContext!!.reply(text).setSuppressEmbeds(suppressEmbeds).queue()

    override fun replyEmbeds(embed: MessageEmbed) = data.eventContext!!.replyEmbeds(embed).queue()
    override fun sendMessage(text: String, suppressEmbeds: Boolean) =
        data.eventContext!!.channel.sendMessage(text).setSuppressEmbeds(suppressEmbeds).queue()

    override fun editLastMessage(text: String) = data.eventContext!!.hook.editOriginal(text).queue()
    override fun isEvent(): Boolean = true

    @Suppress("UNCHECKED_CAST")
    override fun <T> getOption(name: String, type: Type): T? {
        val optionMapping: OptionMapping = data.eventContext!!.getOption(name) ?: return null
        return when (type) {
            String::class.java, java.lang.String::class.java -> optionMapping.asString as T
            Int::class.java, Integer::class.java, Number::class.java, java.lang.Number::class.java -> optionMapping.asInt as T
            Long::class.java, java.lang.Long::class.java -> optionMapping.asLong as T
            Double::class.java, java.lang.Double::class.java -> optionMapping.asDouble as T
            Boolean::class.java, java.lang.Boolean::class.java -> optionMapping.asBoolean as T
            GuildChannelUnion::class.java -> optionMapping.asChannel as T
            Message.Attachment::class.java -> optionMapping.asAttachment as T
            else -> throw IllegalArgumentException("Unknown type $type for option $name")
        }
    }

    override fun hasOption(name: String): Boolean = data.eventContext!!.getOption(name) != null
    override fun getAudioPlayerManager(): AudioPlayerManager? = data.audioManager
    override fun getTrackScheduler(): TrackScheduler? = data.trackScheduler
    override fun getAudioPlayer(): AudioPlayer? = data.audioPlayer
    override fun getCalledUser(): Member? = data.eventContext?.member
}
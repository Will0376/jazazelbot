package ru.will0376.jazazelbot.discord.context

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import ru.will0376.jazazelbot.music.TrackScheduler

data class ContextData(
    val eventContext: SlashCommandInteractionEvent?,
    val audioManager: AudioPlayerManager?,
    val trackScheduler: TrackScheduler?,
    val audioPlayer: AudioPlayer?
)

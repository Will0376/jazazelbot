package ru.will0376.jazazelbot.discord.context

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.entities.channel.unions.MessageChannelUnion
import ru.will0376.jazazelbot.music.TrackScheduler
import java.lang.reflect.Type

interface IDiscordCommandContext {
    fun getGuildId(): Long
    fun getGuild(): Guild
    fun getChannelId(): Long
    fun getChannel(): MessageChannelUnion
    fun getJDA(): JDA
    fun sendReply(text: String, suppressEmbeds: Boolean = false)
    fun setDeferReply()
    fun replyEmbeds(embed: MessageEmbed)
    fun sendMessage(text: String, suppressEmbeds: Boolean = false)
    fun editLastMessage(text: String)

    /**
     * @return true if this context is SlashCommandEventContext
     */
    fun isEvent(): Boolean
    fun <T> getOption(name: String, type: Type): T?
    fun hasOption(name: String): Boolean
    fun getAudioPlayerManager(): AudioPlayerManager?
    fun getTrackScheduler(): TrackScheduler?
    fun getAudioPlayer(): AudioPlayer?
    fun getCalledUser(): Member?
}
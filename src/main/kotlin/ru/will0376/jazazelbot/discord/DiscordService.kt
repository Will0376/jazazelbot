package ru.will0376.jazazelbot.discord

import jakarta.annotation.PostConstruct
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.interactions.commands.build.Commands
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.will0376.jazazelbot.discord.slashs.ISlashCommand
import ru.will0376.jazazelbot.enums.BotChannelType
import ru.will0376.jazazelbot.utils.AppConfig
import ru.will0376.jazazelbot.utils.SlashCommand

@Service
class DiscordService {

    @Autowired
    private lateinit var discordCommandProcessor: DiscordCommandProcessor

    @Autowired
    private lateinit var appConfig: AppConfig

    @Autowired
    lateinit var slashClassList: List<ISlashCommand>

    @Autowired
    lateinit var eventListener: EventListener

    lateinit var jda: JDA

    @PostConstruct
    fun initBot() {
        appConfig.discordToken
            ?: throw IllegalStateException("Discord token is null! Set it in <.jar folder>/settings.properties (field discord.token)")
        val jda = JDABuilder.createDefault(appConfig.discordToken).build()
        jda.awaitReady()
        jda.updateCommands().addCommands(getAllCommands()).queue()
        jda.addEventListener(discordCommandProcessor, eventListener)
        this.jda = jda
    }

    fun getAllCommands(): List<SlashCommandData> {
        return slashClassList.map {
            val annotation = it::class.java.getAnnotation(SlashCommand::class.java)
            annotation
                ?: throw IllegalStateException("Annotation @SlashCommand is not found on class ${it::class.java}")
            Commands.slash(annotation.name, annotation.description).apply {
                isGuildOnly = annotation.channelType == BotChannelType.GUILD
                annotation.options.forEach { option ->
                    addOption(option.type, option.name, option.description, option.required, option.autoComplete)
                }
            }
        }.toList()
    }
}
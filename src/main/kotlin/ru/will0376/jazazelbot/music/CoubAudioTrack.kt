package ru.will0376.jazazelbot.music

import com.sedmelluq.discord.lavaplayer.container.mpeg.MpegAudioTrack
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManager
import com.sedmelluq.discord.lavaplayer.tools.Units
import com.sedmelluq.discord.lavaplayer.tools.io.PersistentHttpStream
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo
import com.sedmelluq.discord.lavaplayer.track.DelegatedAudioTrack
import com.sedmelluq.discord.lavaplayer.track.playback.LocalAudioTrackExecutor
import java.net.URI

class CoubAudioTrack(trackInfo: AudioTrackInfo, val sourceManager: CoubSourceManager) : DelegatedAudioTrack(trackInfo) {
    override fun process(executor: LocalAudioTrackExecutor) {
        sourceManager.getHttpInterface().use { httpInterface ->
            PersistentHttpStream(httpInterface, URI(trackInfo.identifier), Units.CONTENT_LENGTH_UNKNOWN).use { stream ->
                if (trackInfo.identifier.contains(".mp4")) {
                    processDelegate(MpegAudioTrack(trackInfo, stream), executor)
                } else if (trackInfo.identifier.contains(".mp3")) {
                    val mp3AudioTrack = CustomMp3AudioTrack(trackInfo, stream)
                    processDelegate(mp3AudioTrack, executor)
                }
            }
        }
    }

    override fun getSourceManager(): AudioSourceManager = sourceManager
    override fun makeShallowClone(): AudioTrack = CoubAudioTrack(trackInfo, sourceManager)
}
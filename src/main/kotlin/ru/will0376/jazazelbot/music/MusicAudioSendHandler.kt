package ru.will0376.jazazelbot.music

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.track.playback.AudioFrame
import net.dv8tion.jda.api.audio.AudioSendHandler
import java.nio.ByteBuffer

class MusicAudioSendHandler(val audioPlayer: AudioPlayer) : AudioSendHandler {
    var lastFrame: AudioFrame? = null
    override fun canProvide(): Boolean = audioPlayer.provide()?.let { lastFrame = it; true } ?: false

    override fun isOpus(): Boolean = true

    override fun provide20MsAudio(): ByteBuffer? = ByteBuffer.wrap(lastFrame?.data)
}
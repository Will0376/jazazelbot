package ru.will0376.jazazelbot.music

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManager
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException
import com.sedmelluq.discord.lavaplayer.tools.io.HttpClientTools
import com.sedmelluq.discord.lavaplayer.tools.io.HttpConfigurable
import com.sedmelluq.discord.lavaplayer.tools.io.HttpInterface
import com.sedmelluq.discord.lavaplayer.track.AudioItem
import com.sedmelluq.discord.lavaplayer.track.AudioReference
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo
import org.apache.http.client.config.RequestConfig
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClientBuilder
import java.io.DataInput
import java.io.DataOutput
import java.io.IOException
import java.util.function.Consumer
import java.util.function.Function
import java.util.regex.Pattern

class CoubSourceManager : AudioSourceManager, HttpConfigurable {

    private val trackUrlPattern = Pattern.compile(REGEX)

    private val httpInterfaceManager = HttpClientTools.createDefaultThreadLocalManager()
    private val objectMapper = ObjectMapper()
    override fun getSourceName(): String = "coub"

    override fun loadItem(manager: AudioPlayerManager, reference: AudioReference): AudioItem? {
        val trackMatcher = trackUrlPattern.matcher(reference.identifier)
        if (trackMatcher.matches()) {
            return loadTrack(trackMatcher.group(1))
        }
        return null
    }

    private fun loadTrack(group: String): AudioItem {
        try {
            getHttpInterface().use {
                it.execute(HttpGet("https://coub.com/api/v2/coubs/$group")).let { response ->
                    response.statusLine.statusCode.let { code ->
                        if (!HttpClientTools.isSuccessWithContent(code)) {
                            throw IOException("Invalid status code for track page: $code")
                        }
                    }
                    response.entity.content.use { stream ->
                        val readTree = objectMapper.readTree(stream)
                        return extractTrack(readTree)
                    }
                }
            }
        } catch (e: IOException) {
            throw FriendlyException(
                "Error occurred when extracting video info.", FriendlyException.Severity.SUSPICIOUS, e
            )
        }
    }

    private fun extractTrack(jsonNode: JsonNode): AudioTrack {
        val title = jsonNode["title"].asText()
        val author = jsonNode["channel"]["title"].asText()
        val lenght = jsonNode["duration"]!!.asDouble().times(1000).toLong()

        val identifier =
            jsonNode["audio_versions"]["template"]?.asText() ?: jsonNode["file_versions"]["share"]["default"].asText()

        val uri = "https://coub.com/view/${jsonNode["permalink"].asText()}"

        val trackInfo = AudioTrackInfo(
            title,
            author,
            lenght,
            identifier,
            false,
            uri,
        )
        return CoubAudioTrack(trackInfo, this)
    }

    override fun isTrackEncodable(track: AudioTrack?): Boolean = true

    override fun encodeTrack(track: AudioTrack?, output: DataOutput?) {
    }

    override fun decodeTrack(trackInfo: AudioTrackInfo, input: DataInput): AudioTrack = CoubAudioTrack(trackInfo, this)

    override fun shutdown() {
    }

    companion object {
        const val REGEX = "^(?:http://|https://|)(?:www\\.|)coub\\.com/view/([0-9a-zA-Z]+)(?:\\?.*|)$"
    }

    override fun configureRequests(configurator: Function<RequestConfig, RequestConfig>?) {
        httpInterfaceManager.configureRequests(configurator)
    }

    override fun configureBuilder(configurator: Consumer<HttpClientBuilder>?) {
        httpInterfaceManager.configureBuilder(configurator)
    }

    fun getHttpInterface(): HttpInterface {
        return httpInterfaceManager.getInterface()
    }
}
package ru.will0376.jazazelbot.music

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason
import ru.will0376.jazazelbot.discord.utils.SchedulerData
import ru.will0376.jazazelbot.logger

class TrackScheduler(val player: AudioPlayer) : AudioEventAdapter() {
    val queue = mutableListOf<AudioTrack>()
    val playedHistory = mutableListOf<CustomAudioTrackInfo>()
    lateinit var schedulerData: SchedulerData
    override fun onPlayerPause(player: AudioPlayer) {
        player.isPaused = true
    }

    override fun onPlayerResume(player: AudioPlayer) {
        player.isPaused = false
    }

    override fun onTrackStart(player: AudioPlayer, track: AudioTrack) {
        playedHistory.add(track.getCustomAudioTrackInfo())
        printTrackInfo(track)

        schedulerData.timerThread?.takeIf { it.isAlive }?.interrupt()
    }

    override fun onTrackEnd(
        player: AudioPlayer, track: AudioTrack, endReason: AudioTrackEndReason
    ) {
        logger.info { "Track ${track.info?.title} ended with reason $endReason" }
        if (endReason.mayStartNext) {
            playNext(lastTrack = track)
        }
    }

    override fun onTrackException(
        player: AudioPlayer, track: AudioTrack, exception: FriendlyException
    ) {
        exception.let {
            logger.catching(exception.cause ?: exception)
        }
        playNext(lastTrack = track)
    }

    override fun onTrackStuck(player: AudioPlayer, track: AudioTrack, thresholdMs: Long) {
        logger.error { "[Guild ${schedulerData.guildId}] Track ${track.info?.title} stuck for $thresholdMs mills" }
    }

    fun playNext(skipRepeat: Boolean = false, lastTrack: AudioTrack? = null) {
        if (queue.isNotEmpty()) {
            handleNonEmptyQueue(skipRepeat, lastTrack)
        } else {
            handleEmptyQueue(skipRepeat, lastTrack)
        }
    }

    private fun handleNonEmptyQueue(skipRepeat: Boolean, lastTrack: AudioTrack? = null) {
        val trackToPlay = queue.removeAt(0)

        if (lastTrack != null && lastTrack.getAudioData().repeat && lastTrack.getAudioData().repeat && !skipRepeat) {
            queue.add(lastTrack.getFullClone())
        }

        player.playTrack(trackToPlay)
    }

    private fun handleEmptyQueue(skipRepeat: Boolean, lastTrack: AudioTrack? = null) {
        if (lastTrack != null && lastTrack.getAudioData().repeat && !skipRepeat) {
            val fullClone = lastTrack.getFullClone()
            player.playTrack(fullClone)
            return
        }

        if (lastTrack != null) player.stopTrack()

        schedulerData.printToChannel("Queue is empty, nothing to play next")
        startAutoKickTimer()
    }

    private fun startAutoKickTimer() {
        schedulerData.timerThread = Thread {
            try {
                schedulerData.printToChannel("The auto-kick timer is active, add a track using the '/play' command, otherwise the bot will disconnect after 10 minutes")
                Thread.sleep(1000 * 60 * 10)
                if (queue.isEmpty()) {
                    schedulerData.printToChannel("Queue is empty, disconnecting")
                    schedulerData.runnableBlock?.let { it() }
                }
            } catch (e: Exception) {
            }
        }.apply {
            start()
        }
    }

    fun queue(track: AudioTrack, hiddenTrack: Boolean = false, repeat: Boolean = false) {
        logger.info { "[Guild ${schedulerData.guildId}] Adding track '${track.info.title}' to queue (${track.info.uri}) (hidden: $hiddenTrack)" }

        track.getAudioData().repeat = repeat
        track.apply {
            val repeatText = if (repeat) "[R]" else ""
            if (hiddenTrack) {
                schedulerData.printToChannel("$repeatText Adding hidden track to queue", true)
                getAudioData().hidden = true
            } else {
                schedulerData.printToChannel(
                    "$repeatText Adding track '${info.title}' to queue (${info.uri})",
                    true
                )
            }
        }

        if (player.playingTrack != null) {
            queue.add(track)
        } else {
            player.playTrack(track)
        }
    }

    private fun printTrackInfo(track: AudioTrack) {
        val trackData = track.getAudioData()
        val trackInfo = track.info

        val prefix = if (trackData.repeat) "[R]" else ""
        val title = if (trackData.hidden) "Hidden track" else trackInfo.title

        schedulerData.printToChannel("$prefix Playing '$title'", true)
    }

    fun dataIsInit(): Boolean = ::schedulerData.isInitialized
}

fun AudioTrack.getAudioData(): AudioTrackData {
    return userData as? AudioTrackData ?: (AudioTrackData().let { userData = it; it })
}

fun AudioTrack.getFullClone(): AudioTrack {
    return makeClone().apply {
        userData = getAudioData().copy()
    }
}

fun AudioTrack.getCustomAudioTrackInfo(): CustomAudioTrackInfo {
    return (info as? CustomAudioTrackInfo) ?: (CustomAudioTrackInfo(
        info, getAudioData()
    ))
}


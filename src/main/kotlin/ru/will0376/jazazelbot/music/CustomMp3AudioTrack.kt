package ru.will0376.jazazelbot.music

import com.sedmelluq.discord.lavaplayer.container.mp3.Mp3AudioTrack
import com.sedmelluq.discord.lavaplayer.container.mp3.Mp3TrackProvider
import com.sedmelluq.discord.lavaplayer.tools.io.SeekableInputStream
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo
import com.sedmelluq.discord.lavaplayer.track.playback.LocalAudioTrackExecutor
import org.slf4j.LoggerFactory

class CustomMp3AudioTrack(trackInfo: AudioTrackInfo, var inputStream: SeekableInputStream) : Mp3AudioTrack(
    trackInfo, inputStream
) {
    var length: Long = 0
    override fun getDuration(): Long {
        return length
    }

    override fun process(localExecutor: LocalAudioTrackExecutor?) {
        val provider = Mp3TrackProvider(localExecutor!!.processingContext, inputStream)
        try {
            provider.parseHeaders()
            log.debug("Starting to play MP3 track {}", identifier)
            println("dur: ${provider.duration}")
            length = provider.duration
            localExecutor.executeProcessingLoop({ provider.provideFrames() }) { timecode: Long ->
                provider.seekToTimecode(
                    timecode
                )
            }
        } finally {
            provider.close()
        }
    }

    companion object {
        val log = LoggerFactory.getLogger(CustomMp3AudioTrack::class.java)
    }
}
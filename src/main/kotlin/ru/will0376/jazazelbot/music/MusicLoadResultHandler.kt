package ru.will0376.jazazelbot.music

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import ru.will0376.jazazelbot.discord.context.IDiscordCommandContext
import ru.will0376.jazazelbot.discord.context.getContext
import ru.will0376.jazazelbot.discord.utils.PlayArgs
import ru.will0376.jazazelbot.logger

class MusicLoadResultHandler(
    val trackScheduler: TrackScheduler,
    val playArgs: PlayArgs,
    val contextCache: IDiscordCommandContext = getContext(),
    val callback: () -> Unit = {}

) : AudioLoadResultHandler {
    override fun trackLoaded(track: AudioTrack) {
        track.apply {
            logger.info { "[Guild ${contextCache.getGuildId()}] Track loaded: '${info.title}'" }
            trackScheduler.queue(this, hiddenTrack = playArgs.hide, repeat = playArgs.repeat)
            contextCache.editLastMessage("Track loaded: ${if (playArgs.hide) "hidden" else info.title}")
            callback()
        }
    }

    override fun playlistLoaded(playlist: AudioPlaylist) {
        playlist.apply {
            logger.info { "[Guild ${contextCache.getGuildId()}] Track-list loaded: $name, ${tracks.size} tracks" }
            contextCache.editLastMessage("Track-list loaded: $name (${tracks.size} tracks)")
            for ((i, track) in tracks.withIndex()) {
                if (i > playArgs.playlistSize && playArgs.playlistSize != -1) {
                    break
                }
                trackScheduler.queue(track, hiddenTrack = playArgs.hide, repeat = playArgs.repeat)
            }
            callback()
        }
    }

    override fun noMatches() {
        contextCache.sendMessage("No matches found")
    }

    override fun loadFailed(exception: FriendlyException?) {
        exception?.let {
            logger.catching(exception.cause ?: exception)
            contextCache.sendMessage("Load failed: ${exception.message}")
        }
    }
}
package ru.will0376.jazazelbot.music

data class AudioTrackData(var hidden: Boolean = false, var repeat: Boolean = false)


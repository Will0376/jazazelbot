package ru.will0376.jazazelbot.music

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers
import dev.lavalink.youtube.YoutubeAudioSourceManager
import dev.lavalink.youtube.clients.*
import dev.lavalink.youtube.clients.skeleton.Client
import org.springframework.stereotype.Component
import ru.will0376.jazazelbot.utils.SlashException


@Component
class GuildPlayerManager {
    val guildPlayerManagers = mutableMapOf<Long, AudioPlayerManager>()
    val guildPlayers = mutableMapOf<Long, AudioPlayer>()
    val guildSchedulers = mutableMapOf<Long, TrackScheduler>()
    fun getOrInitPlayerManager(guildId: Long): AudioPlayerManager = guildPlayerManagers.getOrPut(guildId) {
        val audioPlayerManager = DefaultAudioPlayerManager()
        audioPlayerManager.registerSourceManager(CoubSourceManager())
        val youtube = YoutubeAudioSourceManager(true,
            WebEmbedded(),Web()
        )

        audioPlayerManager.registerSourceManager(youtube)
        AudioSourceManagers.registerRemoteSources(audioPlayerManager)
        audioPlayerManager
    }

    fun findPlayer(guildId: Long): AudioPlayer {
        return guildPlayers[guildId] ?: throw SlashException("Player is not initialized")
    }

    fun findScheduler(guildId: Long): TrackScheduler {
        return guildSchedulers[guildId] ?: throw SlashException("Scheduler is not initialized")
    }
}
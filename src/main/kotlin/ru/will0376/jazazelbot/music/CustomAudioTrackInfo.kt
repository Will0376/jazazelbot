package ru.will0376.jazazelbot.music

import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo

class CustomAudioTrackInfo(
    title: String,
    author: String,
    length: Long,
    identifier: String,
    isStream: Boolean,
    uri: String,
    var audioTrackData: AudioTrackData
) : AudioTrackInfo(title, author, length, identifier, isStream, uri) {
    constructor(audioTrackInfo: AudioTrackInfo, audioTrackData: AudioTrackData) : this(
        audioTrackInfo.title,
        audioTrackInfo.author,
        audioTrackInfo.length,
        audioTrackInfo.identifier,
        audioTrackInfo.isStream,
        audioTrackInfo.uri,
        audioTrackData
    )
}
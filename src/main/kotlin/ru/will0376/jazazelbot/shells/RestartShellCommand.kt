package ru.will0376.jazazelbot.shells

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import ru.will0376.jazazelbot.discord.slashs.ConnectCommand
import ru.will0376.jazazelbot.music.GuildPlayerManager
import java.util.*
import kotlin.concurrent.timerTask
import kotlin.system.exitProcess
import kotlin.time.DurationUnit
import kotlin.time.toDuration

@ShellComponent
class RestartShellCommand {
    @Autowired
    lateinit var connectCommand: ConnectCommand

    @Autowired
    lateinit var guildPlayerManager: GuildPlayerManager

    @ShellMethod("Restart bot")
    fun restart(): String {
        connectCommand.restartPlanned = true
        Timer().schedule(timerTask { exitProcess(0) }, 1.toDuration(DurationUnit.MINUTES).inWholeMilliseconds)
        guildPlayerManager.guildSchedulers.forEach { (_, u) ->
            u.schedulerData.channel.sendMessage("Bot will be restarted in 1 minute").complete()
        }
        return "Restart planned in 1 minute"
    }
}
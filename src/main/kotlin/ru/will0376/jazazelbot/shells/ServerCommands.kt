package ru.will0376.jazazelbot.shells

import org.jline.utils.AttributedString
import org.jline.utils.AttributedStyle
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.shell.jline.PromptProvider

@ConfigurationProperties(prefix = "shell")
class ServerCommands : PromptProvider {
    override fun getPrompt(): AttributedString =
        AttributedString(":>", AttributedStyle.DEFAULT.foreground(AttributedStyle.GREEN))
}
package ru.will0376.jazazelbot.shells

import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import kotlin.system.exitProcess

@ShellComponent
class StopShellCommand {
    @ShellMethod("Stopping bot")
    fun stop(): String {
        exitProcess(0)
        return "Bye!"
    }
}
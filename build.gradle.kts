import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "3.1.0"
    id("io.spring.dependency-management") version "1.1.0"
    id("com.gorylenko.gradle-git-properties") version "2.4.1"
    kotlin("jvm") version "1.8.21"
    kotlin("plugin.spring") version "1.8.21"
    kotlin("plugin.jpa") version "1.8.21"
}

group = "ru.will0376"
version = "1.0.0"
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
    mavenCentral()
    maven {
        url = uri("https://maven.scijava.org/content/repositories/public/")
    }
    maven {
        url = uri("https://m2.dv8tion.net/releases")
    }
    maven {
        url = uri("https://jitpack.io")
    }
    maven {
        url = uri("https://jcenter.bintray.com/")
    }
    maven("https://maven.arbjerg.dev/releases")
    maven("https://maven.lavalink.dev/releases")
    maven {
        name = "arbjergDevSnapshots"
        url = uri("https://maven.lavalink.dev/snapshots")
    }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.thymeleaf.extras:thymeleaf-extras-springsecurity6")
    developmentOnly("org.springframework.boot:spring-boot-devtools")
    runtimeOnly("com.h2database:h2")
    implementation("net.dv8tion:JDA:5.0.0-beta.10")
//    implementation("com.github.Walkyst.lavaplayer-fork:lavaplayer:custom-SNAPSHOT")
//    implementation("dev.arbjerg:lavaplayer:2.1.1")
    implementation("dev.arbjerg:lavaplayer:2.2.2")
    implementation("dev.lavalink.youtube:v2:1.11.3")
    implementation("io.github.microutils:kotlin-logging-jvm:3.0.5")
    implementation("com.jagrosh:jda-utilities:3.0.5")
    implementation("org.springframework.shell:spring-shell-starter:3.1.0")
    implementation("org.jline:jline:3.22.0")
    //gson
    implementation("com.google.code.gson:gson:2.8.8")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "17"
    }
}

configure<com.gorylenko.GitPropertiesPluginExtension> {
    dateFormat = "yyyy-MM-dd HH:mm"
    (gitPropertiesResourceDir as DirectoryProperty).set(file("src/main/resources"))
}

gitProperties {
    gitPropertiesName = "buildinfo.properties"
    failOnNoGitDirectory = false
    version = "1.1"
}

tasks.withType<Copy> {
    duplicatesStrategy = DuplicatesStrategy.INCLUDE
}

allOpen {
    annotation("jakarta.persistence.Entity")
}